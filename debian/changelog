libauthen-sasl-cyrus-perl (0.13-server-13) unstable; urgency=medium

  * Team upload.
  * Remove Cyrus.pod during clean, it's generated during build.
    This not only fixes the permission problem at build time but also enables
    building-twice-in-a-row. (Closes: #1013602)
  * Refresh lintian override.
  * debian/rules: remove override_dh_auto_test which was just running
    dh_auto_test after a previous change.
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Jun 2022 17:48:26 +0200

libauthen-sasl-cyrus-perl (0.13-server-12) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Remove <!nocheck> from libpod-parser-perl.
    It is needed for the actual build.
  * Update use of variables in debian/rules.
  * Remove $Config{vendorarch} from debian/rules.
    Causes issues with cross-builds, and can be replaced by
    `find … | xargs …'.
  * Add lintian override.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 05 Nov 2021 17:29:09 +0100

libauthen-sasl-cyrus-perl (0.13-server-11) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.
  * Add libpod-parser-perl to build dependencies.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 May 2020 15:03:24 +0200

libauthen-sasl-cyrus-perl (0.13-server-10) unstable; urgency=medium

  * Team upload.
  * debian/rules: update override_dh_fixperms to use vendorarch dir as a
    preparation for multi-arched perl 5.20.
  * Use debhelper 9.20120312 to get default hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Mon, 02 Jun 2014 20:14:47 +0200

libauthen-sasl-cyrus-perl (0.13-server-9) unstable; urgency=medium

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Russ Allbery]
  * Enable bindnow hardening flags.
  * Use the standard INT2PTR macro to convert passed Perl object values to
    the underlying Cyrus SASL struct.  This shouldn't change the generated
    code, but it will fix build warnings and make it easier to find real
    build issues.
  * Update Homepage to point to the author's CPAN directory, since the
    author's home page appears to have gone away.  (This package is of a
    fork of the default CPAN version that adds server support, so the
    normal metacpan URL is not appropriate.)
  * Update standards version to 3.9.5.
    - Update debian/copyright to copyright-format 1.0.
  * Update debhelper compatibility level to V9.

 -- Russ Allbery <rra@debian.org>  Wed, 05 Mar 2014 13:46:47 -0800

libauthen-sasl-cyrus-perl (0.13-server-8) unstable; urgency=low

  * Explicitly set USER when running the test suite, since it may not be
    set in a buildd environment.  This fix was accidentally dropped in
    0.13-server-6.

 -- Russ Allbery <rra@debian.org>  Sat, 05 Mar 2011 22:56:44 -0800

libauthen-sasl-cyrus-perl (0.13-server-7) unstable; urgency=low

  * Modify some type declarations in the XS code to ensure that data types
    match the size that Perl expects on 64-bit platforms.
  * Modify the test suite to report failures with warn rather than print,
    so the error message will be visible outside the test framework.

 -- Russ Allbery <rra@debian.org>  Sat, 05 Mar 2011 21:54:26 -0800

libauthen-sasl-cyrus-perl (0.13-server-6) unstable; urgency=low

  * Change the internal module version to 0.13.1 as a Debian-specific
    workaround to avoid warnings in Perl 5.10 and more serious problems
    with Perl 5.12 with non-numeric versions.  When upstream releases a
    new version without the -server suffix, this change can be dropped.
    (Closes: #578551)
  * Fix an upstream override of manifypods that suppressed generation of a
    man page for the module.
  * Fix syntax errors in the POD documentation previously hidden by our
    failure to install a manual page.
  * Update debhelper compatibility level to V8.
    - Use overrides instead of partial rule implementations.
  * Change to Debian source format 3.0 (quilt).
    - Remove now-unnecessary README.source.
  * Convert debian/copyright to the proposed DEP-5 format.
  * Update standards version to 3.9.1 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sat, 05 Mar 2011 20:29:00 -0800

libauthen-sasl-cyrus-perl (0.13-server-5) unstable; urgency=low

  * Add the missing $(QUILT_STAMPFN) dependency to debian/rules so that
    patches are applied correctly during the build.  Thanks, Ben
    Poliakoff.  (Closes: #538296)
  * Update standards version to 3.8.2 (no changes required).

 -- Russ Allbery <rra@debian.org>  Fri, 24 Jul 2009 10:05:02 -0700

libauthen-sasl-cyrus-perl (0.13-server-4) unstable; urgency=low

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Russ Allbery ]
  * The tied file handle implementation in Authen::SASL::Cyrus::Security
    passed along the return value of print in its WRITE implementation.
    This is always 1, and WRITE is supposed to behave like syswrite and
    return the number of octets written.  Patch from Sergio Gelato.
    (Closes: #486698)
  * Remove extraneous *.patch extension from the output encoding patch and
    add an explanatory introductory comment.
  * Update to debhelper compatibility level V7.
    - Use partial debhelper rules minimization.
  * Update standards version to 3.8.1 (no changes required).

 -- Russ Allbery <rra@debian.org>  Tue, 17 Mar 2009 16:13:08 -0700

libauthen-sasl-cyrus-perl (0.13-server-3) unstable; urgency=low

  * debian/rules:
    + move test suitr to build-stamp target
    + stop setting INSTALLVENDOR* vars, remove usr/share/perl5 if it exists
    + remove unused dh_link
    + no need to chmod -R $(CURDIR) in clean
  * Move the change to Security.pm (encoding data in chunks) to a separate
    patch. Bring quilt into play
  * Add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Wed, 16 Jan 2008 15:40:51 +0200

libauthen-sasl-cyrus-perl (0.13-server-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.

  [ Russ Allbery ]
  * Tell Makefile.PL not to create /usr/share/perl5 and remove the
    rmdir, which fails with Perl 5.10.
  * Use DESTDIR rather than PREFIX when installing.
  * Update standards version to 3.7.3 (no changes required).
  * Update debhelper compatibility level to V5 (no changes required).
  * Wrap Build-Depends for readability.

 -- Russ Allbery <rra@debian.org>  Sat, 29 Dec 2007 15:31:53 -0800

libauthen-sasl-cyrus-perl (0.13-server-1) unstable; urgency=low

  * New upstream release.
  * Change maintainers to the Debian Perl Group.
  * Update debian/copyright and debian/watch to point to CPAN rather than
    the (not updated) home page.
  * debian/rules cleanup.
    - Add build-arch and build-indep targets just in case.
    - Use a better method of optionally running make distclean.
    - Remove stamp files first in clean target.
    - Improve target dependencies.
  * Update standards version to 3.7.2 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sun,  7 May 2006 16:03:50 -0700

libauthen-sasl-cyrus-perl (0.12-server-4) unstable; urgency=low

  * Encode large amounts of data in chunks of SASL_MAXOUTBUF rather than
    trying to encode all the data at once.
  * Providing binary-indep in debian/rules is required by Policy even if
    there are no arch-independent packages.  Whoops.

 -- Russ Allbery <rra@debian.org>  Wed,  7 Dec 2005 12:42:42 -0800

libauthen-sasl-cyrus-perl (0.12-server-3) unstable; urgency=low

  * Use a better technique for optionally running make realclean.
  * Remove unused rules in debian/rules.
  * Reformat debian/copyright and add an explicit copyright and license
    for the Debian packaging.
  * Update maintainer address.

 -- Russ Allbery <rra@debian.org>  Sat, 19 Nov 2005 16:30:35 -0800

libauthen-sasl-cyrus-perl (0.12-server-2) unstable; urgency=low

  * Redo how library data is handled following the recommendations in
    perlxs(1), eliminating a cast of a pointer through an int and thereby
    hopefully fixing problems on 64-bit platforms.
  * Explicitly set USER when running the test suite.  The value doesn't
    matter for the tests, and some package build methods (debuild) strip
    the environment.
  * Update standards version to 3.6.2 (no changes required).
  * Reduce priority to extra again after consultation with ftp-masters.

 -- Russ Allbery <rra@stanford.edu>  Wed, 29 Jun 2005 17:08:35 -0700

libauthen-sasl-cyrus-perl (0.12-server-1) unstable; urgency=low

  * Adopt orphaned package.  (Closes: #279776)
  * Acknowledge NMU.  (Closes: #250520, #288569)
  * Switch upstreams to the -server varient maintained by Patrick
    Boettcher.  It implements considerably more of the API and is
    sufficient for stable use with Net::LDAP for GSSAPI authentication,
    which the CPAN version is not.
  * Reorganize debian/rules, set CFLAGS correctly, add an explicit install
    target, and remove unnecessary debhelper invocations.
  * Improve the description and include a Homepage link.
  * Increase priority to optional.
  * Add build-depends on libauthen-sasl-perl and libsasl2-modules for
    running tests.
  * Update standards version to 3.6.1 (no changes required).
  * Update copyright to include the packaging history and the copyright
    and upstream details for the -server varient.
  * Add a watch file.

 -- Russ Allbery <rra@stanford.edu>  Sun, 12 Jun 2005 16:10:13 -0700

libauthen-sasl-cyrus-perl (0.12-1) unstable; urgency=low

  * QA Upload
  * Changed Maintainer to Debian QA Group <packages@qa.debian.org>
  * Changed Section from 'interpreters' to 'perl' to match override.
  * Added dependency on libauthen-sasl-perl, closes: #288569.
  * New upstream version - various bug fixes - closes: #250520.

 -- Stephen Quinney <stephen@jadevine.org.uk>  Wed,  2 Mar 2005 17:30:38 +0000

libauthen-sasl-cyrus-perl (0.07-1) unstable; urgency=low

  * New Maintainer

 -- Stephen Zander <gibreel@debian.org>  Mon, 26 May 2003 21:53:35 -0700

libauthen-sasl-cyrus-perl (0.05-2) unstable; urgency=low

  * Orphan this package.
  * Run dh_installdocs so that the copyright file gets installed.

 -- Mark Brown <broonie@debian.org>  Thu, 15 May 2003 09:10:02 +0100

libauthen-sasl-cyrus-perl (0.05-1) unstable; urgency=low

  * New upstream release.
  * I think this version now does encryption with the SASL libraries
    -- if not, feel free to open the bug again. (Closes: #144130)
  * Update Standards-Version.

 -- Graeme Mathieson <mathie@wossname.org.uk>  Sun, 29 Sep 2002 16:14:28 +0100

libauthen-sasl-cyrus-perl (0.02-1.1) unstable; urgency=low

  * NMU: Rebuilt against perl 5.8.

 -- Ben Burton <benb@acm.org>  Sun,  1 Sep 2002 15:48:33 +1000

libauthen-sasl-cyrus-perl (0.02-1) unstable; urgency=low

  * Initial Release.

 -- Graeme Mathieson <graeme@mathie.cx>  Thu, 22 Nov 2001 15:50:41 +0000
